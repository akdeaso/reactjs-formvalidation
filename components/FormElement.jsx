import React, { useState } from "react";

const FormElement = () => {
  const [formData, setFormData] = useState({
    nama: "",
    username: "",
    password: "",
    jenisKelamin: "",
    pekerjaan: "",
    alamat: "",
  });

  const [errors, setErrors] = useState({});

  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    setFormData({
      ...formData,
      [name]: value,
    });

    if (errors[name]) {
      setErrors({
        ...errors,
        [name]: null,
      });
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const newErrors = validateForm(formData);
    if (Object.keys(newErrors).length === 0) {
      console.log("Data yang dikirim:", formData);
    } else {
      setErrors(newErrors);
    }
  };

  const validateForm = (data) => {
    const errors = {};

    if (!data.nama) {
      errors.nama = "Nama harus diisi.";
    } else if (!/^[a-zA-Z '.-]*$/.test(data.nama)) {
      errors.nama = "Nama tidak boleh mengandung symbol atau angka.";
    }
    if (!data.username) {
      errors.username = "Username harus diisi.";
    } else if (/[\s!@#$%^&*()_+={}\[\]:;"'<>,.?~\\/]/.test(data.username)) {
      errors.username = "Username tidak boleh mengandung spasi atau symbol.";
    }
    if (!data.password) {
      errors.password = "Password harus diisi.";
    } else if (data.password.length < 6) {
      errors.password = "Password minimal 6 karakter.";
    }
    if (!data.jenisKelamin) {
      errors.jenisKelamin = "Jenis kelamin harus dipilih.";
    }
    if (!data.pekerjaan) {
      errors.pekerjaan = "Pekerjaan harus dipilih.";
    }
    if (!data.alamat) {
      errors.alamat = "Alamat harus diisi.";
    }

    return errors;
  };

  return (
    <div>
      <h1>Form Registrasi</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Nama:{" "}
          <input
            type="text"
            name="nama"
            value={formData.nama}
            onChange={handleInputChange}
          />
          {errors.nama && <ul style={{ color: "red" }}>{errors.nama}</ul>}
        </label>
        <br />
        <label>
          Username:{" "}
          <input
            type="text"
            name="username"
            value={formData.username}
            onChange={handleInputChange}
          />
          {errors.username && (
            <ul style={{ color: "red" }}>{errors.username}</ul>
          )}
        </label>
        <br />
        <label>
          Password:{" "}
          <input
            type="password"
            name="password"
            value={formData.password}
            onChange={handleInputChange}
          />
          {errors.password && (
            <ul style={{ color: "red" }}>{errors.password}</ul>
          )}
        </label>
        <br />
        <label>
          Jenis Kelamin:{" "}
          <div>
            <label>
              <input
                type="radio"
                name="jenisKelamin"
                value="laki-laki"
                checked={formData.jenisKelamin === "laki-laki"}
                onChange={handleInputChange}
              />{" "}
              Laki-laki
            </label>
            <label>
              <input
                type="radio"
                name="jenisKelamin"
                value="perempuan"
                checked={formData.jenisKelamin === "perempuan"}
                onChange={handleInputChange}
              />{" "}
              Perempuan
            </label>
          </div>
          {errors.jenisKelamin && (
            <ul style={{ color: "red" }}>{errors.jenisKelamin}</ul>
          )}
        </label>
        <br />
        <label>
          Pekerjaan:{" "}
          <select
            name="pekerjaan"
            value={formData.pekerjaan}
            onChange={handleInputChange}
          >
            <option value="">Pilih pekerjaan</option>
            <option value="programmer">Programmer</option>
            <option value="desainer">Desainer</option>
            <option value="manajer">Manajer</option>
            <option value="lainnya">Lainnya</option>
          </select>
          {errors.pekerjaan && (
            <ul style={{ color: "red" }}>{errors.pekerjaan}</ul>
          )}
        </label>
        <br />
        <label>
          Alamat:{" "}
          <textarea
            name="alamat"
            value={formData.alamat}
            onChange={handleInputChange}
          />
          {errors.alamat && <ul style={{ color: "red" }}>{errors.alamat}</ul>}
        </label>
        <br />
        <button type="submit">Daftar</button>
      </form>
    </div>
  );
};

export default FormElement;
